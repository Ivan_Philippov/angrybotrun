﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AICollector : MonoBehaviour
{
    private List<Collectable> collectables = new List<Collectable>();

    [SerializeField] internal Movement.AnimatorSettings animatorSettings = new Movement.AnimatorSettings();
    private IMotion playerAnimation;


    private IEnumerator Start()
    {
        yield return new WaitForSeconds(1f);

        playerAnimation = new Movement(animatorSettings);

        collectables.AddRange(FindObjectsOfType<Coin>());

        StartCoroutine(Collect());
    }

    private Collectable GetClosestCollectable()
    {
        float distanceMin = collectables.Min(a => Vector3.Distance(a.transform.position, transform.position));

        return collectables.Find(a => Vector3.Distance(a.transform.position, transform.position) <= distanceMin);
    }


    private IEnumerator Collect()
    {
        var drive = Vector3.forward;

        while (collectables != null && collectables.Count > 0)
        {
            Collectable collectable = GetClosestCollectable();

            var heading = collectable.transform.position - transform.position;
            var distance = heading.magnitude;
            var direction = heading / distance;


            transform.LookAt(direction + transform.position);

            playerAnimation.Move(drive.x, drive.z);

            if (!collectable.gameObject.activeSelf)
                collectables.Remove(collectable);

            yield return new WaitForFixedUpdate();
        }

        playerAnimation.Move(0f, 0f);
        FindObjectOfType<GameManager>().useMover = true;
        Destroy(this);
    }
}
