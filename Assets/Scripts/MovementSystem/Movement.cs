﻿using UnityEngine;

public class Movement : IMotion
{
    private AnimatorSettings animatorSettings;

    internal Movement(AnimatorSettings _animatorSettings)
    {
        animatorSettings = _animatorSettings;
    }

    public void Move(float horizontal, float vertical)
    {
        animatorSettings.animator.SetFloat(animatorSettings.hashSpeed, vertical);
        animatorSettings.animator.SetFloat(animatorSettings.hashDirection, horizontal);

        animatorSettings.animator.speed = animatorSettings.animationSpeed;
    }

    public void Stop()
    {
        animatorSettings.animator.SetFloat(animatorSettings.hashSpeed, 0f);
        animatorSettings.animator.SetFloat(animatorSettings.hashDirection, 0f);
    }

    [System.Serializable]
    internal class AnimatorSettings
    {
        public Animator animator;
        public float animationSpeed;
        internal readonly int hashSpeed = Animator.StringToHash("Speed");
        internal readonly int hashDirection = Animator.StringToHash("Direction");
    }
}

public interface IMotion
{
    void Move(float horizontal, float vertical);
    void Stop();
}