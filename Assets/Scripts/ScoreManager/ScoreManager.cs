﻿using UnityEngine;

public class ScoreManager : MonoBehaviour, IScoreCollector, IScore
{
    private int scores = 0;

    public void Awake()
    {
        GameManager.onStartNewGame += NewGame;
    }

    public void OnDestroy()
    {
        GameManager.onStartNewGame -= NewGame;
    }

    private void NewGame()
    {
        scores = 0;
    }

    public void CollectCoin(int count)
    {
        scores += count;
    }

    public int ScorePerLevel(bool refresh)
    {
        int scoresForCurrentLevel = scores;

        if (refresh)
            scores = 0;

        return scoresForCurrentLevel;
    }
}

internal interface IScoreCollector
{
    void CollectCoin(int count);
}

internal interface IScore
{
    int ScorePerLevel(bool refresh);
}