﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private UIManagerSettings settings = new UIManagerSettings();
    [SerializeField] private ScoreManager score;

    public void Restart(System.Action callback)
    {
        if (callback != null)
        {
            settings.Play.onClick.RemoveAllListeners();
            settings.Play.onClick.AddListener(() => callback.Invoke());
        }
    }

    private void Loose()
    {
        settings.GameOverHolder.SetActive(true);
        settings.MainHolder.SetActive(true);
    }

    private void Win()
    {
        settings.WinGameHolder.SetActive(true);
        settings.MainHolder.SetActive(true);

        settings.scoresText.text = "Your score: " + score.ScorePerLevel(false).ToString();
    }

    private void NewGame()
    {
        settings.GameOverHolder.SetActive(false);
        settings.WinGameHolder.SetActive(false);
        settings.MainHolder.SetActive(false);

        score.ScorePerLevel(true);

        settings.scoresTextInGame.text = "Score: " + score.ScorePerLevel(false).ToString();
    }

    private void OnCollectCollectable(Collectable collectable)
    {
        settings.scoresTextInGame.text = "Score: " + score.ScorePerLevel(false).ToString();
    }

    private void Awake()
    {
        GameManager.onTimerEnd += Loose;
        EndPoint.onPlayerWin += Win;
        GameManager.onStartNewGame += NewGame;
        Collector.onCollectableCollect += OnCollectCollectable;
    }

    private void OnDestroy()
    {
        GameManager.onTimerEnd -= Loose;
        EndPoint.onPlayerWin += Win;
        GameManager.onStartNewGame -= NewGame;
        Collector.onCollectableCollect -= OnCollectCollectable;
    }

    [System.Serializable]
    internal class UIManagerSettings
    {
        [SerializeField] internal GameObject GameOverHolder;
        [SerializeField] internal GameObject WinGameHolder;
        [SerializeField] internal Text scoresText;
        [SerializeField] internal Text scoresTextInGame;
        [SerializeField] internal GameObject MainHolder;
        [SerializeField] internal Button Play;
    }
}
