﻿using UnityEngine;

public class Collectable : MonoBehaviour, ICollectable
{
    [SerializeField] protected int Scores;


    public virtual int Collect()
    {
        return Scores;
    }

    public virtual Collectable GetCollectable()
    {
        return this;
    }
}

public interface ICollectable
{
    int Collect();
    Collectable GetCollectable();
}
