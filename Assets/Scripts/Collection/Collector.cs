﻿using UnityEngine;

public delegate void OnCollectableCollect(Collectable collectable);

public class Collector : MonoBehaviour
{
    private LayerMask mask;
    private IScoreCollector scoreCollector;
    private CoinsPool pool;

    public static OnCollectableCollect onCollectableCollect;

    internal void InitializeCollector(IScoreCollector collector, CoinsPool _pool)
    {
        scoreCollector = collector;
        pool = _pool;
    }

    private void Start()
    {
        mask = 1 << 8;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other != null && mask.value == (mask | (1 << other.gameObject.layer)))
        {
            ICollectable collectable = other.gameObject.GetComponentInChildren<ICollectable>();

            if (collectable != null)
            {
                scoreCollector.CollectCoin(collectable.Collect());
                onCollectableCollect?.Invoke(collectable.GetCollectable());
                pool.ReturnToPool(collectable.GetCollectable());
            }
            else
                Debug.LogError("ICollectable didn't find!!! Visit this class or Collectable object.");
        }
    }
}
