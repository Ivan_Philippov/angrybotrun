﻿using UnityEngine;
using UnityEngine.UI;

public class Timer : ITimer, ICheckTime
{
    private TimerSettings timerSettings;

    private float secondsLeft = 0f;

    internal Timer(TimerSettings _timerSettings)
    {
        timerSettings = _timerSettings;
    }

    public void InitializeTimer()
    {
        secondsLeft = timerSettings.secondsForGameMatch;
    }

    public bool IsTimeOver()
    {
        return secondsLeft <= 0f;
    }

    public void Tick()
    {
        secondsLeft -= Time.fixedDeltaTime;

        if (secondsLeft < 0) secondsLeft = 0.00f;

        timerSettings.timerText.text = Mathf.CeilToInt(secondsLeft).ToString();
    }

    [System.Serializable]
    internal class TimerSettings
    {
        [SerializeField] internal Text timerText;
        [SerializeField] internal float secondsForGameMatch;
    }
}

internal interface ITimer
{
    void InitializeTimer();
    void Tick();
}

internal interface ICheckTime
{
    bool IsTimeOver();
}
