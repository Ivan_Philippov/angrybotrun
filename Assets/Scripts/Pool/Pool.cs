﻿using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Pool<T> where T : Component
{
    [SerializeField] protected T prefab;
    [SerializeField] protected Transform parent;
    [SerializeField, HideInInspector] protected Queue<T> pooledObjects = new Queue<T>();

    internal virtual T Get()
    {
        if (pooledObjects.Count == 0)
            Add(1);

        T obj = pooledObjects.Dequeue();

        obj.transform.parent = null;
        obj.gameObject.SetActive(true);

        return obj;
    }

    internal virtual void Add(int count)
    {
        for (int i = 0; i < count; i++)
        {
            T obj = Component.Instantiate(prefab, parent);
            obj.gameObject.SetActive(false);
            pooledObjects.Enqueue(obj);
        }
    }

    internal virtual void ReturnToPool(T obj)
    {
        obj.gameObject.SetActive(false);
        obj.gameObject.transform.SetParent(parent);
        pooledObjects.Enqueue(obj);
    }

    internal bool EmptyPool()
    {
        return pooledObjects.Count == 0;
    }
}
