﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GeneratorZone : MonoBehaviour
{
    [SerializeField, Range(1, 10)] internal int countItemsinThisRoom = 1;
    [SerializeField] internal Vector3 center = Vector3.zero;
    [SerializeField] internal Vector3 size = Vector3.one;
    [SerializeField] internal bool ShowRoom = true;
    [SerializeField] internal Color gizmoColor = new Color(0, 150, 255, 100);

    List<Collectable> inZone = new List<Collectable>();

    private CoinsPool pool;
    private ICheckTime timer;

    internal void InitializeGenerator(CoinsPool _pool, ICheckTime _timer)
    {
        pool = _pool;
        timer = _timer;
    }

    private void OnDrawGizmos()
    {
        if (ShowRoom)
        {
            Gizmos.color = gizmoColor;
            Gizmos.DrawCube(transform.localPosition + center, size);
        }
    }

    private void Awake()
    {
        GameManager.onStartNewGame += StartGame;
    }

    private void Start()
    {
        Collector.onCollectableCollect += Check;
    }

    private void OnDestroy()
    {
        Collector.onCollectableCollect -= Check;
        GameManager.onStartNewGame -= StartGame;
    }

    private void StartGame()
    {
        StopAllCoroutines();
        StartCoroutine(Generator());
    }

    private void Check(Collectable collectable)
    {
        if (inZone != null && inZone.Count > 0)
        {
            if (!inZone.Exists(a => a == collectable)) return;
            else
            {
                Collectable inList = inZone.Find(a => a == collectable);
                inZone.Remove(inList);
            }
        }
        else return;
    }

    private void Generate(Vector3 room)
    {
        if (IsFreeSpot())
        {
            Collectable collectable = pool.Get();

            collectable.transform.SetParent(transform);

            float x = Random.Range(center.x - size.x / 2, center.x + size.x / 2);
            float z = Random.Range(center.z - size.z / 2, center.z + size.z / 2);
            float y = room.y;

            collectable.transform.localPosition = new Vector3(x, y, z);

            inZone.Add(collectable);
        }
        else return;
    }

    private IEnumerator Generator()
    {
        Vector3 room = transform.localPosition;

        while (!timer.IsTimeOver())
        {
            if (IsFreeSpot())
                Generate(room);
            else yield break;

            yield return null;
        }

        ResetCoinsGeneratedPreviously();
    }

    private void ResetCoinsGeneratedPreviously()
    {
        foreach (var item in inZone)
        {
            pool.ReturnToPool(item);
        }

        inZone.Clear();
    }

    private bool IsFreeSpot()
    {
        return inZone.Count < countItemsinThisRoom;
    }
}
