﻿using UnityEngine;

public delegate void OnPlayerWin();

public class EndPoint : MonoBehaviour
{
    internal static OnPlayerWin onPlayerWin;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.isTrigger)
        {
            onPlayerWin?.Invoke();
        }
    }
}
