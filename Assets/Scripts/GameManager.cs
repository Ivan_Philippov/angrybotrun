﻿using System.Collections;
using UnityEngine;

public delegate void OnStartNewGame();
public delegate void OnTimerEnd();


public class GameManager : MonoBehaviour
{
    [SerializeField] internal Movement.AnimatorSettings animatorSettings = new Movement.AnimatorSettings();
    [SerializeField] internal CameraFollow.CameraFollowSettings cameraFollowSettings = new CameraFollow.CameraFollowSettings();
    [SerializeField] internal Timer.TimerSettings timerSettings = new Timer.TimerSettings();

    [SerializeField] private Settings settings = new Settings();
    [SerializeField] private Joystick joystick;
    [SerializeField] private ScoreManager scoreManager;
    [SerializeField] private Collector playerCollector;
    [SerializeField] private CoinsPool pool = new CoinsPool();
    [SerializeField] private UIManager buttonRestart;
    [SerializeField] private GeneratorZone[] generators;

    public bool useMover = false;

    private IMotion playerAnimation;
    private IFollow follower;
    private ITimer timer;
    private ICheckTime checkTime;


    private Vector3 startingPlayerPosition;
    private Vector3 startingCameraPosition;
    private Quaternion startingPlayerRotation;
    private Quaternion startingCameraRotation;

    internal static OnTimerEnd onTimerEnd;
    internal static OnStartNewGame onStartNewGame;

    private void OnValidate()
    {
        if (pool.EmptyPool()) pool.Add(100);
    }

    private void Awake()
    {
        InitializeGame();
    }

    private void Start()
    {
        EndPoint.onPlayerWin += Resetup;
        InitializeStarting();
        timer.InitializeTimer();
        StartCoroutine(Game());
        onStartNewGame?.Invoke();
    }

    private void OnDestroy()
    {
        EndPoint.onPlayerWin -= Resetup;
    }

    private void InitializeGame()
    {
        playerAnimation = new Movement(animatorSettings);
        follower = new CameraFollow(cameraFollowSettings);

        Timer gameTimer = new Timer(timerSettings);
        timer = gameTimer;
        checkTime = gameTimer;

        playerCollector.InitializeCollector(scoreManager, pool);

        for (int i = 0; i < generators.Length; i++)
        {
            generators[i].InitializeGenerator(pool, gameTimer);
        }
    }

    private IEnumerator Game()
    {
        onStartNewGame?.Invoke();

        while (!checkTime.IsTimeOver())
        {
            if (useMover)
                playerAnimation.Move(joystick.Horizontal, joystick.Vertical);

            follower.Follow(Camera.main.transform);
            timer.Tick();

            yield return new WaitForFixedUpdate();
        }
        onTimerEnd?.Invoke();
        Resetup();
    }

    private void Resetup()
    {
        StopAllCoroutines();
        playerAnimation.Stop();
        buttonRestart.Restart(PlayBack);
    }

    private void InitializeStarting()
    {
        startingPlayerPosition = settings.Player.position;
        startingPlayerRotation = settings.Player.rotation;

        startingCameraPosition = Camera.main.transform.position;
        startingCameraRotation = Camera.main.transform.rotation;
    }

    private void InitializeOnEnd()
    {
        settings.Player.position = startingPlayerPosition;
        settings.Player.rotation = startingPlayerRotation;

        Camera.main.transform.position = startingCameraPosition;
        Camera.main.transform.rotation = startingCameraRotation;
    }

    internal void PlayBack()
    {
        StopAllCoroutines();

        InitializeOnEnd();

        timer.InitializeTimer();

        StartCoroutine(Game());
    }


    [System.Serializable]
    internal class Settings
    {
        [SerializeField] internal Transform Player;
    }
}
