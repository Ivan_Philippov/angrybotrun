﻿using UnityEngine;

public class CameraFollow : IFollow
{
    private CameraFollowSettings settings;

    internal CameraFollow(CameraFollowSettings _settings)
    {
        settings = _settings;
    }

    public void Follow(Transform follower)
    {
        Vector3 lookOnObject = settings.player.position - follower.position;

        follower.forward = lookOnObject.normalized;

        Vector3 playerLastPosition = settings.player.position - lookOnObject.normalized * settings.distance;

        playerLastPosition.y = settings.player.position.y + settings.distance / 2;

        follower.position = Vector3.Slerp(follower.position, playerLastPosition, settings.speed * Time.fixedDeltaTime);
    }

    [System.Serializable]
    internal class CameraFollowSettings
    {
        [SerializeField] internal Transform player;
        [SerializeField] internal float distance;
        [SerializeField] internal float speed;
    }
}

public interface IFollow
{
    void Follow(Transform follower);
}
